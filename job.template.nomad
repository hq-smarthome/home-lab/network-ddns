job "network-ddns" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  vault {
    policies = ["api-cloudflare"]
  }

  group "carboncollins.se" {
    count = 1

    task "hq" {
      driver = "docker"

      config {
        image = "oznu/cloudflare-ddns:latest"
      }

      resources {
        cpu = 100
        memory = 10
      }

      template {
        data = <<EOH
          {{ with secret "api/cloudflare/zones/carboncollins.se" }}
          API_KEY={{ index .Data.data "token" }}
          ZONE=carboncollins.se
          SUBDOMAIN=hq
          {{ end }}
        EOH

        destination = "secrets/cloudflare.env"
        env = true
      }
    }
  }

  group "carboncollins.uk" {
    task "hq" {
      driver = "docker"

      config {
        image = "oznu/cloudflare-ddns:latest"
      }

      resources {
        cpu = 100
        memory = 10
      }

      template {
        data = <<EOH
          {{ with secret "api/cloudflare/zones/carboncollins.uk" }}
          API_KEY={{ index .Data.data "token" }}
          ZONE=carboncollins.uk
          SUBDOMAIN=hq
          {{ end }}
        EOH

        destination = "secrets/cloudflare.env"
        env = true
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
